# This is delete junk characters in Emacs *shell* buffer
if [[ $TERM == "dumb" ]]; then
    # This shell runs inside an Emacs *shell* buffer.
    unsetopt zle
fi
