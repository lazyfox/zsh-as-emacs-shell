#Fix zsh in Emacs output junk characters
Emacs doesn't play nice with ZLE, so we must fix it :)

##Installation

Just source in your .zshrc or put to .oh-my-zsh/custom/plugins if using
[oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh).And add plugin
in *plugins* variable in your .zshrc
```shell
plugins=(... my-zsh-function)
```

